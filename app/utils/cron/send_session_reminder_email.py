import os,sys
from sqlalchemy import Column, Integer, String, DateTime, Text, func, event, and_, exists, or_, not_
from sqlalchemy.orm import aliased

from app import app, db
from ...models.email_message import EmailMessage
from ...models import user, c_event_reservation, c_event, c_event_session
from app.models.time_zone import TimeZone
from ...components import utilities
from ...components import date_time_helpers

import argparse
from datetime import datetime, date, time, timedelta
import pytz


### HOW TO RUN ###
# cd /var/www/vpeng_task && python -m app.utils.cron.send_session_reminder_email

def getAllSessionParticipantsBetween(date_time_lower, date_time_upper):
    now = utilities.now()

    User = user.User
    EventReservation = c_event_reservation.EventReservation
    EventSession = c_event_session.EventSession
    SessionBeingChecked = aliased(EventSession, name='SessionBeingChecked')
    Event = c_event.Event

    # We get all event sessions that are starting after date_time_lower and before date_time_upper
    # And don't have a session happening earlier on the same calendar day
    reservation_results = db.session.query(
        EventSession,
        Event,
        User,
        TimeZone
    ).\
    join(
        Event,
        Event.id == EventSession.event_id
    ).\
    outerjoin(
        TimeZone,
        Event.time_zone_id == TimeZone.id
    ).\
    join(
        EventReservation,
        and_(
            EventReservation.event_id == EventSession.event_id,
            EventReservation.deleted == 0
        )
    ).\
    join(
        User,
        EventReservation.user_id == User.id
    ).\
    filter(
        EventSession.session_end_time != None,
        EventSession.session_start_time >= date_time_lower,
        EventSession.session_start_time < date_time_upper,
        not_(db.session.query(
                SessionBeingChecked
            ).filter(
                SessionBeingChecked.id != EventSession.id,
                SessionBeingChecked.session_date == EventSession.session_date,
                SessionBeingChecked.session_end_time != None,
                SessionBeingChecked.session_start_time <= EventSession.session_start_time
            ).exists()
        )
    ).\
    group_by(
        EventSession.event_id,
        User.id
    ).\
    all()

    users = []
    # Need to track unique user-events that have been added to the users list
    user_event_ids = set()

    for row in reservation_results:
        if (row.User.id, row.Event.id) not in user_event_ids:
            if row.TimeZone:
                time_zone = {
                    'id': row.TimeZone.id,
                    'name': row.TimeZone.name,
                    'pytz_timezone': row.TimeZone.pytz_timezone,
                    'is_default': False
                }

            row_user = row.User.toDict()
            row_user['session_info'] = row.EventSession.toDict()
            row_user['event'] = row.Event.toDict()
            row_user['timezone'] = time_zone
            user_event_ids.add((row.User.id, row.Event.id))
            users.append(row_user)

    return users


def send_mail_for_attendees(dryRun=True):
    # How many days ahead to look for sending the emails
    days_ahead = 1
    # If the hour in local time on a 24hour clock is equal to the target_hour
    # then all events in the next calendar day will be sent
    target_hour = 6

    # this the time that the cron will run. add ` +/- timedelta(hours=xxx)` to see what it would like to sent at another time
    date_time_sending = datetime.datetime.now(app.timezone)

    # establish the lower bound (inclusive) for getting the correct events.
    # finally adjust for the target hour. since we want to send these at a specific hour after midnight
    # ex. if the event starts at 10AM CST on 7/15, we want the email to go out at 6AM CST on 7/14,
    # so we go back 12:00AM for CST, which is 1:00AM EST/EDT, and so on
    date_time_lower = date_time_sending - datetime.timedelta(hours=target_hour)
    # we add # of days to the time it's being sent
    date_time_lower = date_time_lower + datetime.timedelta(days=days_ahead)
    # we then flatten the time down to the hour (in case the time sent is not on the hour, every hour, and/or is a little off)
    date_time_lower = date_time_lower.replace(minute=0, second=0, microsecond=0)

    # establish upper bound (exclusive) which is 1 day ahead of the lower bound
    date_time_upper = date_time_lower + datetime.timedelta(years=1)

    # get users that have sessions between lower and upper
    users = getAllSessionParticipantsBetween(date_time_lower, date_time_upper)

    # some helpful messages when dry run
    if dryRun:
        app.logger.error('Bounds: {} - {}'.format(date_time_lower, date_time_upper))
        app.logger.error('Found {} users'.format(len(users)))

    for user in users:
        # Create the session time string, based on the user's timezone
        pytz_timezone = pytz.timezone(user.get('timezone').get('pytz_timezone'))
        event = user.get('event')
        session = user.get('session_info')

        session_time = date_time_helpers.formatEventSessionTime(
            session.get('session_start_time'),
            session.get('session_end_time'),
            pytz_timezone
        )

        url_prefix = app.config.get('BETTERLESSON_URL')

        if dryRun:
            data = {
                'user name': user.get('first_name'),
                'user id': user.get('id'),
                'session name': session.get('name'),
                'session id': session.get('id'),
                'event name': event.get('display_name'),
                'event_id': event.get('id'),
                'eventTime': session_time,
                'reset code': user.get('reset_code')
            }
            app.logger.error(data)
        else:
            # add the reset code to the button link if the user needs to set their password
            resetCode = '&code=' + user.get('reset_code') if user.get('reset_code') else ''
            # email notification
            notificationData = {
                'template_id': app.config['SENDGRID_TEMPLATE_IDS']['event_reminder'],
                'toEmail': user.get('email'),
                'fromEmail': 'BetterLesson <noreply@betterlesson.com>',
                'replyTo': 'BetterLesson Support <support@betterlesson.atlassian.net>',
                'subject': 'test email {}'.format(session_time),
                'type': 'event_reminder',
                'dynamic_template_data': {
                    'participantFirstName': user.get('first_name'),
                    'eventTime': session_time,
                    'eventName': event.get('display_name'),
                    'checkInLink': '{}/lab/event/{}/register'.format(url_prefix, event.get('id')),
                    'resetCode': resetCode
                }
            }
            email = EmailMessage(notificationData)
            email.sendTransactionalEmail(userId=user.get('id'))

##################################################################################
# Main
##################################################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--run',
        action='store_const',
        const=True,
        help='Whether to send the emails'
    )
    args = parser.parse_args()
    dryRun = not args.run

    print 'Sending reminder emails for participants.'

    send_mail_for_attendees(dryRun=dryRun)

    print 'Mission accomplished, hopefully!'
