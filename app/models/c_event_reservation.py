#!/usr/bin/python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, DateTime, Text, func, event, and_, or_, exists, distinct
from sqlalchemy.dialects.mysql import \
        BIGINT, BINARY, BIT, BLOB, BOOLEAN, CHAR, DATE, \
        DATETIME, DECIMAL, DECIMAL, DOUBLE, ENUM, FLOAT, INTEGER, \
        LONGBLOB, LONGTEXT, MEDIUMBLOB, MEDIUMINT, MEDIUMTEXT, NCHAR, \
        NUMERIC, NVARCHAR, REAL, SET, SMALLINT, TEXT, TIME, TIMESTAMP, \
        TINYBLOB, TINYINT, TINYTEXT, VARBINARY, VARCHAR, YEAR

# These are RSVPs
class EventReservation():
    __tablename__ = 'bl_c_event_reservations'

    id = Column(INTEGER, primary_key=True)
    created = Column(DATETIME, nullable=False)
    lastmodified = Column(TIMESTAMP, nullable=False)
    event_id = Column(INTEGER, nullable=False)
    user_id = Column(INTEGER, nullable=False)
    deleted = Column(TINYINT(1), nullable=False, default=0)
