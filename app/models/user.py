#!/usr/bin/python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, DateTime, Text, func, event, and_, or_, exists, distinct
from sqlalchemy.dialects.mysql import \
        BIGINT, BINARY, BIT, BLOB, BOOLEAN, CHAR, DATE, \
        DATETIME, DECIMAL, DECIMAL, DOUBLE, ENUM, FLOAT, INTEGER, \
        LONGBLOB, LONGTEXT, MEDIUMBLOB, MEDIUMINT, MEDIUMTEXT, NCHAR, \
        NUMERIC, NVARCHAR, REAL, SET, SMALLINT, TEXT, TIME, TIMESTAMP, \
        TINYBLOB, TINYINT, TINYTEXT, VARBINARY, VARCHAR, YEAR

class User():
    __tablename__ = 'bl_user'

    id = Column(INTEGER, primary_key=True)
    created = Column(DATETIME, nullable=False)
    lastmodified = Column(TIMESTAMP, nullable=False)
    email = Column(VARCHAR(64), nullable=False)
    password = Column(VARCHAR(45), nullable=False)
    first_name = Column(VARCHAR(45), nullable=True)
    last_name = Column(VARCHAR(45), nullable=True)
    reset_code = Column(VARCHAR(45), nullable=True)
    time_zone_id = Column(INTEGER, nullable=True)
