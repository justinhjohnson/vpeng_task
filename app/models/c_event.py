#!/usr/bin/python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, DateTime, Text, func, event, and_, or_, exists, distinct
from sqlalchemy.dialects.mysql import \
        BIGINT, BINARY, BIT, BLOB, BOOLEAN, CHAR, DATE, \
        DATETIME, DECIMAL, DECIMAL, DOUBLE, ENUM, FLOAT, INTEGER, \
        LONGBLOB, LONGTEXT, MEDIUMBLOB, MEDIUMINT, MEDIUMTEXT, NCHAR, \
        NUMERIC, NVARCHAR, REAL, SET, SMALLINT, TEXT, TIME, TIMESTAMP, \
        TINYBLOB, TINYINT, TINYTEXT, VARBINARY, VARCHAR, YEAR

class Event():
    __tablename__ = 'bl_c_events'

    id = Column(INTEGER, primary_key=True)
    created = Column(DATETIME, nullable=False)
    lastmodified = Column(TIMESTAMP, nullable=False)
    name = Column(VARCHAR(200), nullable=True)
    display_name = Column(VARCHAR(200), nullable=True)
    description = Column(VARCHAR(500), nullable=True)
    status = Column(VARCHAR(50), nullable=True)
    service = Column(VARCHAR(50), nullable=True)
    max_capacity = Column(INTEGER(10), nullable=True)
    partner_id = Column(INTEGER, nullable=True)
    time_zone_id = Column(INTEGER, nullable=True)
    facilitator_user_id = Column(INTEGER(10), nullable=True)
    deleted = Column(TINYINT(1), nullable=False, default=0)
