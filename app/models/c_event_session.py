#!/usr/bin/python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, DateTime, Text, func, event, and_, or_, exists, distinct
from sqlalchemy.dialects.mysql import \
        BIGINT, BINARY, BIT, BLOB, BOOLEAN, CHAR, DATE, \
        DATETIME, DECIMAL, DECIMAL, DOUBLE, ENUM, FLOAT, INTEGER, \
        LONGBLOB, LONGTEXT, MEDIUMBLOB, MEDIUMINT, MEDIUMTEXT, NCHAR, \
        NUMERIC, NVARCHAR, REAL, SET, SMALLINT, TEXT, TIME, TIMESTAMP, \
        TINYBLOB, TINYINT, TINYTEXT, VARBINARY, VARCHAR, YEAR

class EventSession():
    __tablename__ = 'bl_c_event_sessions'

    id = Column(INTEGER, primary_key=True)
    created = Column(DATETIME, nullable=False)
    lastmodified = Column(TIMESTAMP, nullable=False)
    name = Column(TEXT, nullable=True)
    session_date = Column(DATE, nullable=True)
    session_start_time = Column(DATETIME, nullable=True)
    session_end_time = Column(DATETIME, nullable=True)
    num = Column(INTEGER, nullable=False)
    event_id = Column(INTEGER, nullable=False)
    deleted = Column(TINYINT(1), nullable=False, default=0)
