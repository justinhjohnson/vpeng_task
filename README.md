Consider a system where users may RSVP (i.e. reserve a spot) for events. Each event consists of one or more scheduled sessions, as per a schema in the attached code. A hypothetical engineer released the contents of `crontab` and `send_session_reminder_email.py` as part of the project described below. There are a handful of errors that are impacting the customer experience.

Please identify and list the errors, and prioritize them for resolution.
Outline a coaching plan for this engineer to resolve the errors.

We've tried to include the minimum code necessary for you to accomplish this task, so this code will not actually run, nor are you expected to get it to run. You may assume that any code not included is correctly implemented.

### Send event session reminder email one day before the session
As a user who RSVP'd for an event, I should receive an email to remind me of an upcoming event session, so that I have the link to check in.

##### Acceptance Criteria:
- Users who RSVP'd should receive a reminder email at 6 AM in the event’s timezone the day before a session.
- For multi-session events, the email reminder should be sent before each session, EXCEPT if there was already a session of that event earlier that day.
- Email data should be sent to Sendgrid, where the email template will get populated.
- We should be able to trigger this email whenever we want on dev for testing purposes. Please consider that when writing the code.
